(function(root, factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD. Register as an anonymous module.
		define(['exports', 'b'], factory);
	} else if (typeof exports === 'object' && typeof exports.nodeName !== 'string') {
		// CommonJS
		factory(exports, require('b'));
	} else {
		// Browser globals
		root.ROCK_SERVICES = root.ROCK_SERVICES || {};
		factory((root.ROCK_SERVICES), root.b);
	}
}(this, function(exports, b) {
	var AuthService = function() {
		this.CONFIG = new Configuration();

		return this;
	};

	AuthService.prototype.signin = function(data, callback, ecallback) {
		ajax({
			type: 'POST',
			url: this.CONFIG.API.signin,
			data: data,
			success: callback,
			error: ecallback
		});
	}

	AuthService.prototype.adminSignin = function(data, callback, ecallback) {
		ajax({
			type: 'POST',
			url: this.CONFIG.API.adminSignin,
			data: data,
			success: callback,
			error: ecallback
		});
	}

	AuthService.prototype.socialsignin = function (network, callback) {
		ajax({
			type: 'POST',
			url: this.CONFIG.API['signin'+network],
			success: callback
		});	
	}

	AuthService.prototype.signup = function(data, callback, ecallback) {
		ajax({
			type: 'POST',
			url: this.CONFIG.API.signup,
			data: data,
			success: callback,
			error: ecallback
		});
	}

	AuthService.prototype.signout = function(token, callback) {
		ajax({
			type: 'POST',
			headers: {
				token: token
			},
			url: this.CONFIG.API.signout,
			success: callback
		});
	}

	AuthService.prototype.changepassword = function(data, token, callback, ecallback) {
		ajax({
			type: 'PUT',
			headers: {
				token: token
			},
			data: data,
			url: this.CONFIG.API.changepassword,
			success: callback,
			error: ecallback
		});
	}

	AuthService.prototype.verifyEmail = function(token, callback, ecallback) {
		ajax({
			type: 'POST',
			url: this.CONFIG.API.verifyemail + '/' + token,
			success: callback,
			error: ecallback
		});
	}

	AuthService.prototype.sendForgotPasswordEmail = function(emailObject, callback, ecallback) {
		ajax({
			type: 'POST',
			url: this.CONFIG.API.sendforgotpasswordemail,
			data: emailObject,
			success: callback,
			error: ecallback
		});
	}


	AuthService.prototype.resetPassword = function(token, passwordObject, callback, ecallback) {
		ajax({
			type: 'POST',
			url: this.CONFIG.API.resetpassword + '/' + token,
			data: passwordObject,
			success: callback,
			error: ecallback
		});
	}
	

	function ajax(args) {
		var response = {
			headers: {},
			data: {}
		};
		var request = new XMLHttpRequest();
		request.open(args.type, args.url, true);
		request.setRequestHeader('content-type', 'application/json');
		if (args.headers && args.headers.token) {
			request.setRequestHeader("x-auth-token", args.headers.token);
		}
		request.onload = function() {
			if (request.status >= 200 && request.status < 400) {
				var token = request.getResponseHeader("X-Auth-Token");
				if (token) {
					response.headers.token = token;
				}
				response.data = JSON.parse(request.responseText || "{}");
				args.success(response, request.status);
			}
			if (request.status >= 400 && request.status <=500) {
				response.data = JSON.parse(request.responseText || "{}");
				if(args.error)
					args.error(response,request.status);
			}
		};
		request.onerror = args.error;
		request.send(args.data ? JSON.stringify(args.data) : null);
	}



	function Configuration() {
		var ver = 1;
		var host = getAPIUrl();
		
		return {
			VER: ver,
			HOST: host,
			API: {
				signin: host + 'rest/user/sign-in',
				adminSignin: host + 'rest/admin/sign-in',
				signout: host + 'signout',
				signup: host + 'rest/user/register/with-captcha',
				signinfacebook: host + 'signin/facebook',
				signinlinkedin: host + 'signin/linkedin',
				signintwitter: host + 'signin/twitter',
				signingoogle: host + 'signin/google',

				verifyemail: host + 'verify',
				sendforgotpasswordemail: host + 'recoverpassword',
				resetpassword: host + 'recoverpassword',
				changepassword: host + 'changepassword',
				initial: host
			}
		};
	}

	function getAPIUrl() {

		
		var url = location.origin + "/";
		
		if(window.location.hostname === "localhost" || window.location.hostname.indexOf("galactica") === -1) {

			url = 'http://localhost:8080/';
        }

		console.log("HOST: ", url);
		return url;
	}
		// var environment = getEnvFromDomain(window.location.hostname);
		// var url;
		// switch(environment) {
		// 	case "DEV":
		// 		url = 'http://galactica-dev.elasticbeanstalk.com/';
		// 		break;
		// 	case "STAGING":
		// 		url = 'http://galactica-staging.elasticbeanstalk.com/';
		// 		break;
		// 	case "PROD":
		// 		url = 'http://galactica-prod.elasticbeanstalk.com/';
		// 		break;
		// 	default:
		// 		url = 'http://galactica-dev.elasticbeanstalk.com/';
		// }
	

	// function getEnvFromDomain(domain) {

	// 	if(domain.indexOf("dev") > -1) {
	// 		return "DEV";
	// 	} else if(domain.indexOf("staging") > -1) {
	// 		return "STAGING";
	// 	} else if(domain.indexOf("prod") > -1) {
	// 		return "PROD";
	// 	} else {
	// 		return "";
	// 	}
	// };

	Configuration.prototype.set = function(config) {
		for (key in config) {
			this[key] = config[key];
			if (typeof config[key] === 'object') {

			}
		}
	};

	// attach properties to the exports object to define
	// the exported module properties.
	exports.auth = new AuthService();
}));
